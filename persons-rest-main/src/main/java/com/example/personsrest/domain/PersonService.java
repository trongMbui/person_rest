package com.example.personsrest.domain;


import com.example.personsrest.remote.GroupRemote;
import com.example.personsrest.remote.GroupRemoteImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;
import java.util.stream.Stream;

@AllArgsConstructor
@Service
public class PersonService {

    PersonRepository personRepository;
    GroupRemote groupRemote;

    public Stream<Person> all(){
     return personRepository.findAll().stream();
    }


    public Person createPerson(String name, int age, String city) {

        PersonEntity personEntity = new PersonEntity(UUID.randomUUID().toString(), name, city, age, new ArrayList<>(), true);

        return personRepository.save(personEntity);
    }

    public Person get(String id) {
        return personRepository.findById(id).get();
    }

    public Person updatePerson(String id, String name, int age, String city) {

        Person person = get(id);
        person.setName(name);
        person.setAge(age);
        person.setCity(city);

        return personRepository.save(person);

    }

    public void delete(String id) {
        personRepository.delete(id);
    }


    public Person addGroupToPerson(String id, String name) {

        //List<String> listGroupNames = new ArrayList<>();

        //PersonEntity personEntity = personRepository.get(id);
        //GroupRemoteImpl.Group json = groupRemote.get(name);

        //personEntity.groups.add(json.getName());

        Person person = get(id);

        //groupRemote.createGroup(name);

        //GroupRemoteImpl.Group group = new GroupRemoteImpl.Group(name, person);


        person.addGroup(groupRemote.createGroup(name));

        return personRepository.save(person);
    }
}
