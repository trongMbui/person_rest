package com.example.personsrest.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

import java.util.List;
@Data
@AllArgsConstructor
public class PersonEntity implements Person{

    String id;
    String name;
    String city;
    int age;
    List<String> groups;
    boolean active;


    @Override
    public void addGroup(String groupId) {
        groups.add(groupId);
    }

    @Override
    public void removeGroup(String groupId) {
        groups.remove(groupId);
    }
}
